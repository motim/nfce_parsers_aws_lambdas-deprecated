from lxml import html

def parse(parsed_html):
    'Parses the HTML from "Produtos / Serviços" tab'
    descriptions = get_descriptions(parsed_html)
    qtds = get_qtds(parsed_html)
    units = get_comercial_units(parsed_html)
    totals = get_total_values(parsed_html)
    products_codes = get_products_codes(parsed_html)
    ncms = get_ncms(parsed_html)
    cfops = get_cfops(parsed_html)
    eans_1 = get_commercial_eans(parsed_html)
    eans_2 = get_taxable_eans(parsed_html)
    unit_values = get_unit_values(parsed_html)

    products = []
    for index, item in enumerate(descriptions):
        products.append(
            {
                'descricao': descriptions[index],
                'qtd': qtds[index],
                'unidade_comercial': units[index],
                'valor': totals[index],
                'valor_unitario': unit_values[index],
                'codigo': products_codes[index],
                'ncm': ncms[index],
                'cfop':cfops[index],
                'ean_comercial': eans_1[index],
                'ean_tributavel': eans_2[index]
            }
        )
    return products

def get_descriptions(parsed_html):
    path_descriptions = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table[@class='toggle']/tbody/tr[@class='highlighted']/td[@class='fixo-prod-serv-descricao']/span[@class='multiline']/text()"
    return parsed_html.xpath(path_descriptions)

def get_qtds(parsed_html):
    path_qtds = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table[@class='toggle']/tbody/tr[@class='highlighted']/td[@class='fixo-prod-serv-qtd']/span[@class='linha']/text()"
    return parsed_html.xpath(path_qtds)

def get_comercial_units(parsed_html):
    path = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table[@class='toggle']/tbody/tr[@class='highlighted']/td[@class='fixo-prod-serv-uc']/span[@class='linha']/text()"
    return parsed_html.xpath(path)

def get_total_values(parsed_html):
    path = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table[@class='toggle']/tbody/tr[@class='highlighted']/td[@class='fixo-prod-serv-vb']/span[@class='linha']/text()"
    return parsed_html.xpath(path)

def get_products_codes(parsed_html):
    path = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[1]/tbody/tr[@class='col-4']/td[1]/span[@class='linha']/text()"
    return parsed_html.xpath(path)

def get_ncms(parsed_html):
    path = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[1]/tbody/tr[@class='col-4']/td[2]/span[@class='linha']/text()"
    return parsed_html.xpath(path)

def get_cfops(parsed_html):
    path = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[1]/tbody/tr[2]/td[2]/span[@class='linha']/text()"
    return parsed_html.xpath(path)

def get_commercial_eans(parsed_html):
    path = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table/tbody/tr[@class='col-3']/td[1]/span[@class='linha']"
    eans = parsed_html.xpath(path)
    eans = [ean.text for ean in eans]
    return eans

def get_taxable_eans(parsed_html):
    path = "//div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[1]/span[@class='linha']"
    eans = parsed_html.xpath(path)
    eans = [ean.text for ean in eans]
    return eans

def get_unit_values(parsed_html):
    path = "////div[@id='Prod']/div/table/tbody/tr/td[@class='table_produtos']/table/tbody/tr/td/table/tbody/tr[4]/td[1]/span[@class='linha']/text()"
    return parsed_html.xpath(path)
