from nfce_html_parser import issuer as issuer_parser
import os.path
from lxml import html

def test_issuer():
    html_file = open(os.path.dirname(__file__) + '/../../emitente.htm')
    html_string = "".join(html_file.readlines())
    html_string = html.fromstring(html_string)
    data = issuer_parser.parse(html_string)
    print(data)
    assert 'razao_social' in data
    assert 'cnpj' in data
    assert 'municipio' in data
    assert 'nome' in data['municipio']
    assert 'numero' in data['municipio']
    assert 'bairro' in data
    assert 'endereco' in data
    assert 'cep' in data