from lxml import html

def parse(parsed_html):
    'Parses the HTML from "Emitente" tab'
    return {
        'razao_social': _get_razao_social(parsed_html),
        'cnpj': _get_cnpj(parsed_html),
        'municipio': _get_municipio_field(parsed_html),
        'bairro': _get_bairro(parsed_html),
        'endereco': _get_endereco(parsed_html),
        'cep': _get_cep(parsed_html)
    }

def _get_razao_social(parsed_html):
    return parsed_html.xpath("//div/table/tbody/tr/td[1]/span[@class='multiline']/text()")[0]

def _get_cnpj(parsed_html):
    return parsed_html.xpath("//div/table[2]/tbody/tr[2]/td[1]/span/text()")[0]

def _get_municipio_field(parsed_html):
    municipio_content = parsed_html.xpath('//*[@id="Emitente"]/table[2]/tbody/tr[4]/td[1]/span/text()')[0]
    municipio_content = municipio_content.strip()
    nome = municipio_content.split('-')[1].strip()
    numero = municipio_content.split('-')[0].strip()
    return {
        'nome': nome,
        'numero': numero
    }

def _get_bairro(parsed_html):
    return parsed_html.xpath("//div[@id='Emitente']/table[2]/tbody/tr[3]/td[1]/span[@class='linha']/text()")[0]

def _get_endereco(parsed_html):
    return parsed_html.xpath("//div[@id='Emitente']/table[2]/tbody/tr[2]/td[2]/span[@class='multiline']/text()")[0].strip().replace('\n','').replace('\xa0','')

def _get_cep(parsed_html):
    return parsed_html.xpath("//div[@id='Emitente']/table[2]/tbody/tr[3]/td[2]/span[@class='linha']/text()")[0]
