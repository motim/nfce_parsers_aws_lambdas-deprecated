from flask import Flask, request, jsonify
from exceptions import InvalidContent, InvalidPayload
from lxml import html
from nfce_html_parser import products as products_parser
from nfce_html_parser import nfe as nfce_parser
from nfce_html_parser import issuer as issuer_parser
import json

app = Flask(__name__)

@app.route("/parse/products", methods=["POST"])
def parse_products():
    products = []
    payload = None
    if request.headers['Content-Type'] == 'text/plain':
        payload = json.loads(request.data)
    elif request.headers['Content-Type'] == 'application/json':
        payload = request.get_json()
    
    if payload:
        try:
            html_string = payload['html_string']
        except KeyError:
            raise InvalidPayload('Invalid payload. You must sent an obj like this: {"html_string": content}')
        
        try:
            nfce_produtos_servicos_html = html.fromstring(html_string)
            products = products_parser.parse(nfce_produtos_servicos_html)
        except Exception:
            raise InvalidContent('Invalid content. The content is not a HTML valid')
    return jsonify({'itens':products})

@app.route("/parse/nfce", methods=["POST"])
def parse_nfce():
    nfce = {}

    if request.headers['Content-Type'] == 'text/plain':
        payload = json.loads(request.data)
    elif request.headers['Content-Type'] == 'application/json':
        payload = request.get_json()

    if payload:
        try:
            html_string = payload['html_string']
            nfce_html = html.fromstring(html_string)
            nfce = nfce_parser.parse(nfce_html)
        except KeyError:
            raise InvalidPayload('Invalid payload. You must sent an obj like this: {"html_string": content}')

    return jsonify({'nfce': nfce})

@app.route("/parse/issuer", methods=["POST"])
def parse_issuer():
    issuer = {}

    if request.headers['Content-Type'] == 'text/plain':
        payload = json.loads(request.data)
    elif request.headers['Content-Type'] == 'application/json':
        payload = request.get_json()

    if payload:
        try:
            html_string = payload['html_string']
            issuer_html = html.fromstring(html_string)
            issuer = issuer_parser.parse(issuer_html)
        except KeyError:
            raise InvalidPayload('Invalid payload. You must sent an obj like this: {"html_string": content}')

    return jsonify({'issuer': issuer})

