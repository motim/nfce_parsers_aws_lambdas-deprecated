import os
import main
import unittest
from lxml import html
import json
import exceptions

class NfeTabTestCases(unittest.TestCase):
    endpoint = "/parse/nfce"
    def setUp(self):
        self.app = main.app.test_client()
        self.app.testing = True 
        self.file = open('nfe_tab.htm')
        self.html_string = "".join(self.file.readlines())

    def test_successful_request(self):
        response = self.app.post(
            self.endpoint, 
            data=json.dumps(dict(
                html_string=self.html_string
            )),
            content_type='application/json'
        );
        assert response.status_code == 200
        data = json.loads(response.get_data())
        assert 'nfce' in data
        assert 'modelo' in data['nfce']
        assert 'serie' in data['nfce']
        assert 'numero' in data['nfce']
        assert 'data' in data['nfce']
        assert 'hora' in data['nfce']
        assert 'valor' in data['nfce']
        assert 'chave_acesso' in data['nfce']
        assert 'emitente' in data['nfce']
        assert 'cnpj' in data['nfce']['emitente']
        assert 'razao_social' in data['nfce']['emitente']
        assert 'ie' in data['nfce']['emitente']
        assert 'uf' in data['nfce']['emitente']

    def test_invalid_payload(self):
        response = self.app.post(
            self.endpoint, 
            data=json.dumps(dict(
                html=self.html_string
            )),
            content_type='application/json'
        );
        self.assertRaises(exceptions.InvalidPayload)

    def tearDown(self):
        self.file.close()

if __name__ == '__main__':
    unittest.main()
