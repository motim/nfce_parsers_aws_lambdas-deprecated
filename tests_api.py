import os
import main
import unittest
from lxml import html
import json
import exceptions

class ProductsTestCases(unittest.TestCase):
    endpoint = "/parse/products"
    def setUp(self):
        self.app = main.app.test_client()
        self.app.testing = True 
        self.nfce_produtos_servicos_file = open('nfce_produtos_servicos.htm')
        self.html_string = "".join(self.nfce_produtos_servicos_file.readlines())

    def test_successful_request(self):
        response = self.app.post(
            self.endpoint, 
            data=json.dumps(dict(
                html_string=self.html_string
            )),
            content_type='application/json'
        );
        assert response.status_code == 200
        data = json.loads(response.get_data())
        assert len(data['itens']) > 0
    
    def test_invalid_html(self):
        response = self.app.post(
            self.endpoint, 
            data=json.dumps(dict(
                html_string='<html></html>'
            )),
            content_type='application/json'
        );
        data = json.loads(response.get_data())
        assert len(data['itens']) == 0
    
    def test_invalid_payload(self):
        response = self.app.post(
            self.endpoint, 
            data=json.dumps(dict(
                html=self.html_string
            )),
            content_type='application/json'
        );
        self.assertRaises(exceptions.InvalidPayload)
        
    def tearDown(self):
        self.nfce_produtos_servicos_file.close()

if __name__ == '__main__':
    unittest.main()
